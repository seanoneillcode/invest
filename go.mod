module invest

go 1.12

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.1
)
