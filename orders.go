
package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	"log"
	"net/http"
)

type Order struct {
	OrderID 		string 	`json:"orderID"`
	AssetName 		string 	`json:"asset-name"`
	InstructionType string 	`json:"instruction-type"`
	Amount 			int64 	`json:"amount"` // stores 79.00 as 7900
}

var orders map[string]Order

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(render.SetContentType(render.ContentTypeJSON))
	router.Get("/", GetAllOrders)
	router.Post("/", AddOrder)
	router.Get("/{orderID}", GetOrder)
	return router
}

func GetAllOrders(w http.ResponseWriter, r *http.Request) {
	render.JSON(w, r, orders)
}

func AddOrder(w http.ResponseWriter, r *http.Request) {
	id := uuid.Must(uuid.NewRandom()).String()
	decoder := json.NewDecoder(r.Body)
	var order Order
	err := decoder.Decode(&order)
	if err != nil {
		log.Fatal("failed to decode create order request")
		return // TODO gracefully handle error, respond to user
	}
	order.OrderID = id
	orders[id] = order
	render.JSON(w, r, order)
}

func GetOrder(w http.ResponseWriter, r *http.Request) {
	orderID := chi.URLParam(r, "orderID")
	if orderID == "" {
		render.JSON(w, r, "no orderID specified")
		return
	}
	order, prs := orders[orderID]
	if !prs {
		render.JSON(w, r, fmt.Sprintf("orderID %s not found", orderID))
		return
	}
	render.JSON(w, r, order)
}

func main() {
	// tmp in-memory data
	orders = make(map[string]Order)
	orders["a7df"] = Order{OrderID:"a7df", AssetName:"GB00BQ1YHQ70", InstructionType:"SELL", Amount:1000}

	router := Routes()

	log.Print("Listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}