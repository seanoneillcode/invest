package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestGetOneItem(t *testing.T) {

	orders = make(map[string]Order)
	orders["a7df"] = Order{OrderID:"a7df", AssetName:"GB00BQ1YHQ70", InstructionType:"SELL", Amount:1000}

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/a7df", nil)
	Routes().ServeHTTP(w, req)

	expectedBytes, _ := json.Marshal(orders["a7df"])
	expected := string(expectedBytes) + "\n"
	actual := w.Body.String()
	if expected != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func TestListNoItem(t *testing.T) {

	orders = make(map[string]Order)

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	Routes().ServeHTTP(w, req)

	expected := "{}\n"
	actual := w.Body.String()
	if expected != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func TestListOneItem(t *testing.T) {

	orders = make(map[string]Order)
	orders["a7df"] = Order{OrderID:"a7df", AssetName:"GB00BQ1YHQ70", InstructionType:"SELL", Amount:1000}

	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	Routes().ServeHTTP(w, req)

	expectedMap := make(map[string]Order)
	expectedMap["a7df"] = orders["a7df"]
	expectedBytes, _ := json.Marshal(expectedMap)
	expected := string(expectedBytes) + "\n"
	actual := w.Body.String()
	if expected != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func TestAddItem(t *testing.T) {

	orders = make(map[string]Order)

	// create order
	originalOrder := Order{AssetName:"GB00BQ1YHQ71", InstructionType:"RAISE", Amount:2000}
	bytes, _ := json.Marshal(originalOrder)
	w := httptest.NewRecorder()
	reader := strings.NewReader(string(bytes))
	req := httptest.NewRequest(http.MethodPost, "/", reader)
	Routes().ServeHTTP(w, req)
	var newOrder Order
	err := json.Unmarshal([]byte(w.Body.String()), &newOrder)
	if err != nil {
		t.Fatalf("failed to unmarshall create order response %s", w.Body.String())
	}

	// test that it is returned
	w = httptest.NewRecorder()
	req = httptest.NewRequest(http.MethodGet, "/", nil)
	Routes().ServeHTTP(w, req)

	expectedMap := make(map[string]Order)
	expectedOrder := Order{OrderID:newOrder.OrderID, AssetName:"GB00BQ1YHQ71", InstructionType:"RAISE", Amount:2000}
	expectedMap[newOrder.OrderID] = expectedOrder
	expectedBytes, _ := json.Marshal(expectedMap)
	expected := string(expectedBytes) + "\n"
	actual := w.Body.String()
	if expected != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}